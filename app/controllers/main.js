import { Food } from "../models/model.js";
import { getInfoFromForm } from "../controllers/controller.js";
import { renderFoodList } from "../controllers/controller.js";

const BASE_URL = "https://6461dfef491f9402f4ac8cfa.mockapi.io/food";

let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      let foodArr = res.data.map((item) => {
        let { name, type, discount, img, desc, price, status, id } = item;
        let food = new Food(id, name, type, price, discount, status, img, desc);
        return food;
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};

let xoaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMonAn = xoaMonAn;

fetchFoodList();

window.themMon = () => {
  let data = getInfoFromForm();  
  let newFood = {
    name: data.tenMon,
    type: data.loai,
    discount: data.khuyenMai,
    img: data.hinhMon,
    desc: data.moTa,
    price: data.giaMon,
    status: data.tinhTrang,
  };
  axios({
    url: BASE_URL,
    method: "POST",
    data: newFood,
  })
    .then((res) => {
      $("#exampleModal").modal("hide");
      fetchFoodList();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.suaMonAn = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      $("#exampleModal").modal("show");
      let { id, type, price, img, status, desc, discount, name } = res.data;
      document.getElementById("foodID").value = id;
      document.getElementById("tenMon").value = name;
      document.getElementById("loai").value = type ? "loai1" : "loai2";
      document.getElementById("giaMon").value = price;
      document.getElementById("khuyenMai").value = discount;
      document.getElementById("tinhTrang").value = status ? "1" : "0";
      document.getElementById("hinhMon").value = img;
      document.getElementById("moTa").value = desc;
    })
    .catch((err) => {
      console.log(err);
    });
};

window.capnhatMonAn = () => {
  let data = getInfoFromForm();

  axios({
    url: `${BASE_URL}/${dataMon.maMon}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchFoodList();
    })
    .catch(function (err) {
      console.log("err:", err);
    });
}