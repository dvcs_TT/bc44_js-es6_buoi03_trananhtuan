export let getInfoFromForm = () => {
  let maMon = document.getElementById("foodID").value;
  let tenMon = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value * 1;
  let khuyenMai = document.getElementById("khuyenMai").value * 1;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;
  return {
    maMon,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
}

export let renderFoodList = (foodArr) => {
  let trHtmlClt = "";
  foodArr.forEach((item) => {
    let { maMon, tenMon, loai, khuyenMai, tinhTrang, giaMon } = item;
    let trHtmlEle = `<tr>
        <td>${maMon}</td>
        <td>${tenMon}</td>
        <td>${loai ? "Chay" : "Mặn"}</td>
        <td>${giaMon}</td>
        <td>${khuyenMai}</td>
        <td>${item.tinhGiaKM()}</td>
        <td>${tinhTrang ? "Còn" : "Hết"}</td>
        <td>
        <button 
        onclick="suaMonAn(${maMon})"
         class="btn btn-primary">Edit</button>
        <button 
        onclick="xoaMonAn(${maMon})"
        class="btn btn-danger">Delete</button>
        </td>
  </tr>`;
    trHtmlClt += trHtmlEle;
  });
    
  document.getElementById("tbodyFood").innerHTML = trHtmlClt;
};